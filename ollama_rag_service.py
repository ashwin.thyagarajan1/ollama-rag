import os

from langchain.retrievers import ContextualCompressionRetriever
from langchain.retrievers.document_compressors import FlashrankRerank
from langchain_chroma import Chroma
from langchain_community.embeddings.sentence_transformer import SentenceTransformerEmbeddings
from langchain_community.llms.ollama import Ollama
from langchain_community.vectorstores.faiss import FAISS
from langchain_core.callbacks import CallbackManager, StreamingStdOutCallbackHandler
from langchain_core.documents import Document
from langchain_core.output_parsers import StrOutputParser
from langchain_core.prompts import ChatPromptTemplate
from langchain_text_splitters import RecursiveCharacterTextSplitter
from pypdf import PdfReader
from streamlit.runtime.uploaded_file_manager import UploadedFile

from question_decomposition_rag_service import QuestionDecompositionRAGService
from vector_store_type import VectorStoreType


class OllamaRAGService:
    def __init__(self, pdf_file: UploadedFile, vector_store_type: VectorStoreType):
        text = self.__get_pdf_text(pdf_file=pdf_file)

        text_splitter = RecursiveCharacterTextSplitter(
            chunk_size=1500,
            chunk_overlap=100,
            length_function=len
        )
        chunks = text_splitter.split_text(text=text)
        self.vector_store = self.__set_vector_store(
            pdf_file=pdf_file,
            chunks=chunks,
            vector_store_type=vector_store_type
        )

    @staticmethod
    def __set_vector_store(pdf_file: UploadedFile, chunks: list[str], vector_store_type: VectorStoreType):
        embedding_function = SentenceTransformerEmbeddings(
            model_name="all-MiniLM-L6-v2"
        )
        if vector_store_type == VectorStoreType.Faiss:
            store_name = os.path.join('stores', pdf_file.name[:-4])
            if os.path.exists(os.path.join(store_name, 'index.pkl')):
                vector_store = FAISS.load_local(
                    store_name,
                    embeddings=embedding_function,
                    allow_dangerous_deserialization=True
                )
            else:
                vector_store = FAISS.from_texts(
                    chunks,
                    embedding=embedding_function
                )
                vector_store.save_local(store_name)
        else:
            store_name = pdf_file.name[:-4]
            vector_store = Chroma.from_texts(
                texts=chunks,
                embedding=embedding_function,
                collection_name=store_name
            )
        return vector_store

    @staticmethod
    def __get_pdf_text(pdf_file: UploadedFile):
        pdf_reader = PdfReader(pdf_file)
        text = ""
        for page in pdf_reader.pages:
            text += page.extract_text()
        return text

    @staticmethod
    def __simple_rag_response(query: str, llm: Ollama, docs: list[Document]):
        prompt_template = """Use the following context as your learned knowledge, inside <context></context> XML tags.
<context>
    {context}
</context>

When answer to user:
- If you don't know, just say that you don't know.
- If you don't know when you are not sure, ask for clarification.
Avoid mentioning that you obtained the information from the context.
And answer according to the language of the user's question.

Given the context information, answer the query.
Query: {question}"""
        prompt = ChatPromptTemplate.from_template(prompt_template)
        qa_chain = prompt | llm | StrOutputParser()
        response = qa_chain.invoke({"context": docs, "question": query})
        return response

    def generate_response(self,
                          query: str,
                          selected_model: str,
                          use_question_decomposition: bool = False,
                          use_individual_answers: bool = False):
        llm = Ollama(
            model=selected_model,
            verbose=True,
            callback_manager=CallbackManager(
                [StreamingStdOutCallbackHandler()]
            ),
            stop=["<|eot_id|>", "<|end_of_text|>"]
        )
        docs = self.vector_store.similarity_search(query=query, k=3)

        if use_question_decomposition:
            retriever = self.vector_store.as_retriever(search_kwargs={"k": 3})
            compressor = FlashrankRerank()
            compression_retriever = ContextualCompressionRetriever(
                base_compressor=compressor,
                base_retriever=retriever
            )
            question_decomposition_rag_service = QuestionDecompositionRAGService(
                llm=llm,
                retriever=compression_retriever,
                docs=docs
            )
            return question_decomposition_rag_service.generate_response(
                query=query,
                use_individual_answers=use_individual_answers
            )
        else:
            return self.__simple_rag_response(
                query=query,
                llm=llm,
                docs=docs
            )
