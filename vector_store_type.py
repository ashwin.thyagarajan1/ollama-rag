from enum import Enum


class VectorStoreType(Enum):
    Faiss = 1
    ChromaDb = 2