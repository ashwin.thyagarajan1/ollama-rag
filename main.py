import streamlit as st

from ollama_rag_service import OllamaRAGService
from vector_store_type import VectorStoreType

st.set_page_config(page_title='PDF Chat Bot | Local LLM')

if "messages" not in st.session_state:
    st.session_state.messages = []


def main():
    st.sidebar.header('Ask questions to your PDF using a locally hosted LLM')

    # Upload PDF file
    pdf = st.sidebar.file_uploader('**Upload your PDF**', type='pdf')
    if pdf is not None:
        rag_service = OllamaRAGService(
            pdf_file=pdf,
            vector_store_type=VectorStoreType.Faiss
        )

        for message in st.session_state.messages:
            with st.chat_message(message["role"]):
                st.markdown(message["content"])

        # Models
        available_models = [
            "llama2:7b",
            "llama2:13b",
            "mistral",
            "gemma:7b",
            "gemma:2b",
            "llama3",
        ]
        selected_model = st.sidebar.selectbox('Select a model', available_models)

        # Question
        query = st.chat_input('Ask your questions here')

        # Ask
        if query is not None:
            with st.chat_message("user"):
                st.markdown(query)
            st.session_state.messages.append({"role": "user", "content": query})

            result = rag_service.generate_response(
                query=query,
                selected_model=selected_model,
                use_question_decomposition=False,
                use_individual_answers=False
            )
            displayed_result = f'**{selected_model}**: {result}'
            with st.chat_message("assistant"):
                st.markdown(displayed_result)
            st.session_state.messages.append({"role": "assistant", "content": displayed_result})


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
