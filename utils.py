import json
from collections.abc import Iterable

from jsonpatch import basestring
from langchain.load import dumps, loads


def get_unique_union(documents: list[list]):
    """ Unique union of retrieved docs """
    # Flatten list of lists, and convert each Document to string
    flattened_docs = [dumps(doc) for sublist in documents for doc in sublist]
    # Get unique documents
    unique_docs = list(set(flattened_docs))
    # Return
    return [loads(doc) for doc in unique_docs]


def parse_json_garbage(s: str):
    """Parses a string to extract the json content from it ignoring any extras at the beginning of the string"""
    s = s[next(idx for idx, c in enumerate(s) if c in "{["):]
    try:
        return json.loads(s)
    except json.JSONDecodeError as e:
        return json.loads(s[:e.pos])


def flatten(coll):
    """Flattens a multidimensional array to a simple array"""
    for i in coll:
        if isinstance(i, Iterable) and not isinstance(i, basestring):
            for subc in flatten(i):
                yield subc
        else:
            yield i


def pretty_print(contents):
    print('\n*****', end='\n')
    print(contents, end='\n')
    print('\n*****', end='\n')
