from operator import itemgetter

from langchain import hub
from langchain_community.llms.ollama import Ollama
from langchain_core.documents import Document
from langchain_core.output_parsers import StrOutputParser
from langchain_core.prompts import ChatPromptTemplate
from langchain_core.retrievers import BaseRetriever

from utils import pretty_print


class QuestionDecompositionRAGService:
    def __init__(self, llm: Ollama, retriever: BaseRetriever, docs: list[Document],):
        self.llm = llm
        self.retriever = retriever
        self.docs = docs

        decomposition_prompt_template = """You are a helpful assistant that generates multiple sub-questions related 
                to an input question.
                Only generate the questions that can be answered with the help of the following context:
                 {context}
                 The goal is to break down the input into a set of sub-problems / sub-questions that can 
                be answers in isolation. Do not include anything else in the response. Output should only contain the 
                generated queries. Generate multiple search queries related to: {question} Output (3 queries):"""
        self.decomposition_prompt = ChatPromptTemplate.from_template(decomposition_prompt_template)

        qa_pair_prompt_template = """Here is a set of Q+A pairs: {context} 
        Use the following as your learned knowledge, inside <context></context> XML tags.
<context>
    {background_knowledge}
</context>
        Use these to synthesize an answer to the question: {question}. 
        
        Do not use any information not available in the context to formulate the answers. 
                When answer to user:
- If you don't know, just say that you don't know.
- If you don't know when you are not sure, ask for clarification.
Avoid mentioning that you obtained the information from the context.
Avoid mentioning Q+A pairs in the answer.
And answer according to the language of the user's question.
                
                """
        self.qa_pair_prompt = ChatPromptTemplate.from_template(qa_pair_prompt_template)

    @staticmethod
    def __format_qa_pair(question, answer):
        """Format Q and A pair"""

        formatted_string = ""
        formatted_string += f"Question: {question}\nAnswer: {answer}\n\n"
        return formatted_string.strip()

    @staticmethod
    def __format_qa_pairs(questions, answers):
        """Format Q and A pairs"""

        formatted_string = ""
        for i, (question, answer) in enumerate(zip(questions, answers), start=1):
            formatted_string += f"Question {i}: {question}\nAnswer {i}: {answer}\n\n"
        return formatted_string.strip()

    def __retrieve_and_rag(self,
                           question,
                           prompt_rag,
                           sub_question_generator_chain,
                           retriever):
        """RAG on each sub-question"""

        # Use question decomposition
        sub_questions = sub_question_generator_chain.invoke({"context": self.docs, "question": question})

        # Initialize a list to hold RAG chain results
        rag_results = []

        for sub_question in sub_questions:
            if len(sub_question) == 0:
                continue
            # Retrieve documents for each sub-question
            retrieved_docs = retriever.invoke(sub_question)
            # self.vector_store.similarity_search(query=sub_question, k=3)
            # retriever.get_relevant_documents(sub_question)

            # Use retrieved documents and sub-question in RAG chain
            qa_chain = prompt_rag | self.llm | StrOutputParser()
            answer = qa_chain.invoke({"context": retrieved_docs, "question": sub_question})
            pretty_print(self.__format_qa_pair(sub_question, answer))
            rag_results.append(answer)

        return rag_results, sub_questions

    def __individually_answered_subquestions_rag_response(self,
                                                          query: str,
                                                          prompt_rag):
        """Generates sub-questions based on the provided question and individually
        answers each of them to provide additional context while answering the main question"""

        generate_queries_decomposition = (self.decomposition_prompt
                                          | self.llm
                                          | StrOutputParser()
                                          | (lambda x: x.split("\n")))
        answers, questions = self.__retrieve_and_rag(
            question=query,
            prompt_rag=prompt_rag,
            sub_question_generator_chain=generate_queries_decomposition,
            retriever=self.retriever
        )
        context = self.__format_qa_pairs(
            questions=questions,
            answers=answers
        )

        rag_chain = self.qa_pair_prompt | self.llm | StrOutputParser()
        response = rag_chain.invoke(
            {
                "context": context,
                "background_knowledge": self.docs,
                "question": query
            }
        )
        return response

    def __use_qa_pairs_rag_response(self,
                                    query: str):
        generate_queries_decomposition = (self.decomposition_prompt
                                          | self.llm
                                          | StrOutputParser()
                                          | (lambda x: x.split("\n")))
        sub_questions = generate_queries_decomposition.invoke(
            {
                "context": self.docs,
                "background_knowledge": self.docs,
                "question": query
            }
        )
        template = """Here is the question you need to answer: \n --- \n {question} \n --- \n Here is any available 
        background question + answer pairs: \n --- \n {q_a_pairs} \n --- \n Here is additional context relevant to 
        the question: \n --- \n {context} \n --- \n Use the above context and any background question + answer pairs 
        to answer the question. If you don't know the answer, just say that you don't know.: \n {question}"""

        decomposition_prompt = ChatPromptTemplate.from_template(template)

        context = ""
        for question in sub_questions:
            if len(question) == 0:
                continue
            # Pass the context which includes the question and answer from the previously answered subquestion
            rag_chain = (
                    {"context": itemgetter("question") | self.retriever,
                     "question": itemgetter("question"),
                     "q_a_pairs": itemgetter("q_a_pairs")}
                    | decomposition_prompt
                    | self.llm
                    | StrOutputParser())
            answer = rag_chain.invoke({"question": question, "q_a_pairs": context})
            q_a_pair = self.__format_qa_pair(question, answer)
            pretty_print(q_a_pair)
            context = context + "\n---\n" + q_a_pair
        final_rag_chain = (self.qa_pair_prompt
                           | self.llm
                           | StrOutputParser())
        response = final_rag_chain.invoke({"context": context, "question": query})
        return response

    def generate_response(self, query: str, use_individual_answers: bool = False, ):
        if use_individual_answers:
            prompt_rag = hub.pull("rlm/rag-prompt")
            return self.__individually_answered_subquestions_rag_response(
                query=query,
                prompt_rag=prompt_rag
            )
        else:
            return self.__use_qa_pairs_rag_response(
                query=query
            )
